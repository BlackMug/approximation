from PyQt5.QtCore import pyqtSignal, QPointF, Qt
from pyqtgraph import ViewBox

from ext_logging import extlog


class ClickableViewBox(ViewBox):
    clicked_here = pyqtSignal(QPointF)

    def __init__(self, parent=None):
        super(ClickableViewBox, self).__init__(parent)

    def mouseClickEvent(self, ev):
        super(ClickableViewBox, self).mouseClickEvent(ev)
        if ev.button() == Qt.LeftButton:
            point = self.mapSceneToView(ev._scenePos)
            extlog.debug("Emmit click at {0}".format(point))
            self.clicked_here.emit(point)
