import math

import time

from ext_logging import extlog


# for bx+c
def calc_linear_bc(linear_data):
    x = linear_data.keys()
    y = linear_data.values()
    n = len(linear_data)

    sumx = sum(x)
    sumy = sum(y)
    sumx2 = sum([xi ** 2 for xi in x])
    sumxy = sum([x * y for x, y in linear_data.items()])

    b = (n * sumxy - sumx * sumy) / \
        (n * sumx2 - sumx ** 2)
    c = (sumy - b * sumx) / \
        n

    return b, c


class ApproxSin:
    a = b = c = d = 0

    mistake = 0
    pirson = 0

    mistake_rate_value = 0.5
    pirson_value = 1-mistake_rate_value

    def __init__(self, data):
        pass
        # data - is a dictionary with values of the form {x: y}
        # self.calc_abcd(data) # Uncomment if you want to plot on the start

    def f(self, x):
        return self.a*math.sin(self.b*x + self.c) + self.d

    def f_mod(self, f):
        return math.asin((f-self.d)/self.a)

    def calc_abcd(self, data):
        if not data:
            extlog.error("No data for calc abcd.")
            return

        start_time = time.time()

        ref_x = list(data.keys())
        ref_y = list(data.values())

        maxx = max(ref_x)
        maxy = max(ref_y)
        minx = min(ref_x)
        miny = min(ref_y)
        rangex = maxx-minx
        rangey = maxy-miny

        # It is assumed, that a is in range of three rangey
        # , since a variate stretching of function in OY coordinates
        froma = miny - rangey
        toa = maxy + rangey
        rangea = toa - froma

        # It is assumed, that d is in range from miny to maxy
        # , since d variate position of function in OY coordinates
        #   and define horizontal center of function
        fromd = miny - rangea/2
        tod = maxy + rangea/2

        eps = 0.000001
        half_eps = eps/2

        n = 100
        while True:
            h = (tod-fromd)/n
            if h <= eps:
                break
            maxval = float("-inf")
            maxd = maxval
            for i in range(n):
                curd = h*i+fromd
                curval = self.calc_val_with_const_d(curd, froma, toa, half_eps, data)
                if curval > maxval:
                    maxval = curval
                    maxd = curd

            fromd = maxd-h
            tod = maxd+h

        # left = -1
        # right = -1
        # previous = 0
        # while tod-fromd > eps:
        #     midd = (fromd + tod)/2
        #     leftd = midd-half_eps
        #     rightd = midd+half_eps
        #
        #     left_val = self.calc_val_with_const_d(leftd, froma, toa, half_eps, data)
        #     right_val = self.calc_val_with_const_d(rightd, froma, toa, half_eps, data)
        #
        #     if left_val <= right_val:
        #         fromd = midd
        #         previous = left
        #     elif left_val > right_val:
        #         tod = midd
        #         previous = right
        #     else:
        #         if previous == left:
        #             tod = midd
        #             previous = right
        #         elif previous == right:
        #             fromd = midd
        #             previous = left

        extlog.debug("Done. Required time: {0}".format(time.time()-start_time))

    def calc_val_with_const_d(self, curd, froma, toa, half_eps, data):
        self.d = curd

        # Except area for a
        area_bounds = max([math.fabs(f - self.d) for f in data.values()])

        cur_val1 = self.variate_a(data, half_eps, froma, -area_bounds)
        backup = (self.a, self.b, self.c, self.mistake, self.pirson)

        cur_val2 = self.variate_a(data, half_eps, area_bounds, toa)

        if cur_val1 > cur_val2:
            self.a, self.b, self.c, self.mistake, self.pirson = backup
            return cur_val1
        else:
            return cur_val2

    def variate_a(self, data, half_eps, moving_froma, moving_toa):
        default = float("-inf")
        cur_val = default
        if moving_toa >= moving_froma:
            while moving_toa - moving_froma > half_eps * 2:
                mida = (moving_froma + moving_toa) / 2
                lefta = mida - half_eps
                righta = mida + half_eps

                self.a = lefta
                linear_data = {x: self.f_mod(y) for x, y in data.items()}
                self.b, self.c = calc_linear_bc(linear_data)
                left_val = self.calc_criterion(data)

                self.a = righta
                linear_data = {x: self.f_mod(y) for x, y in data.items()}
                self.b, self.c = calc_linear_bc(linear_data)
                right_val = self.calc_criterion(data)

                if right_val > left_val:
                    moving_froma = mida
                else:
                    moving_toa = mida

                cur_val = right_val
        return cur_val

    def calc_criterion_cached(self):
        val1 = ((1 - self.mistake) * self.mistake_rate_value)
        val2 = ((self.pirson/2+1) * self.pirson_value)
        val = val1+val2
        return val

    def calc_criterion(self, data):
        self.mistake = self.calc_mistake(data)
        self.pirson = self.calc_pirson(data)
        if not (-1 < self.pirson < 1):
            extlog.error("HMM. Pirson: {0}".format(self.pirson))
        if not (0 < self.mistake < 1):
            extlog.error("HMM. Mistake: {0}".format(self.mistake))
        return self.calc_criterion_cached()

    def calc_mistake(self, data):
        # Returns value in [0, 1]

        # return sum([(self.f(x)-y)**2 for x,y in data.items()])

        # return self.strip(
        #     0,
        #     sum([math.fabs(self.f(x)-y) for x, y in data.items()]) / \
        #     sum([math.fabs(y) for y in data.values()]),
        #     1
        # )

        return sum([math.fabs(self.f(x)-y) for x, y in data.items()]) / \
               sum([math.fabs(y) for y in data.values()])

        # return sum([self.f(x)-y for x, y in data.items()]) / \
        #         sum([y for y in data.values()])

        # return sum([(self.f(x)-y)**2 for x, y in data.items()]) / \
        #         sum([y**2 for y in data.values()])

    @staticmethod
    def strip(min_val, val, max_val):
        if val < min_val:
            return min_val
        if val > max_val:
            return max_val
        return val

    def calc_pirson(self, data):
        # Returns value in [0, 1]

        x = list(data.keys())
        y = list(data.values())
        f = [self.f(xi) for xi in x]
        n = len(data)

        avgf = sum(f)/n
        avgy = sum(y)/n

        diffy = [yi-avgy for yi in y]
        difff = [fi-avgf for fi in f]

        try:
            return \
                sum([diffy[i] * difff[i] for i in range(n)]) / \
                math.sqrt(sum([dyi**2 for dyi in diffy]) * sum([dfi**2 for dfi in difff]))
        except ZeroDivisionError:
            extlog.error("Very strange. Seems like you have wrong calculations or IDEAL regression.")
            return 0

    def set_a(self, a):
        self.a = a

    def set_b(self, b):
        self.b = b

    def set_c(self, c):
        self.c = c

    def set_d(self, d):
        self.d = d

    def set_mistake_rate(self, mistake_rate):
        self.mistake_rate_value = mistake_rate
        self.pirson_value = 1-self.mistake_rate_value
