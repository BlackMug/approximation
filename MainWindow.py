from CusromViewBox import ClickableViewBox
from ext_logging import extlog
import os

import pyqtgraph as pg
from PyQt5 import uic
from PyQt5.QtCore import QObject, QFileSystemWatcher, pyqtSlot, QPointF
from DataPlotHandler import *


class SignalBlocker:
    blocked_objects = []

    def __init__(self, objects):
        self.blocked_objects = objects
        for obj in self.blocked_objects:
            obj.blockSignals(True)

    def __del__(self):
        for obj in self.blocked_objects:
            obj.blockSignals(False)


class MainWindow(QObject):
    # Constants
    _data_file = u"input.txt"

    # Variables
    _ui = None
    _plot_widget = None
    _dph = None
    _watcher = None
    _view_box = None

    def __init__(self, ui_file, parent=None, *args):
        super().__init__(parent=parent)
        if (ui_file is None) or (not os.path.exists(ui_file)):
            error_message = "Ui file {0} not initialized or not exist.".format(str(ui_file))
            extlog.error(error_message)
            raise FileNotFoundError(error_message)

        extlog.info("File {0}.".format(os.path.abspath(__file__)))

        self._view_box = ClickableViewBox()
        self._plot_widget = pg.PlotWidget(viewBox=self._view_box)
        self._plot_widget.showGrid(True, True, 0.6)
        self._dph = DataPlotHandler(self.get_plot_item(), parent=self)

        self._watcher = QFileSystemWatcher()
        self._watcher.addPath(self._data_file)

        self.init_ui(ui_file)
        self.init_signals_and_slots()

        input_data = self.read_data_from_file(self._data_file)
        if not input_data:
            extlog.error("No valid data on input. Exiting...")
            exit(-1)
        self._dph.set_input_data(input_data)
        self.balance_rate(self._dph.get_mistake_rate()*self.balance_total, init_call=True)

    def read_data_from_file(self, filename):
        try:
            lines = open(filename, 'r').readlines()
            return dict(
                [(lambda x, y: (float(x), float(y)))(*pair.split(' ')) for pair in lines]
            )
        except TypeError:
            extlog.error("File contains not available symbols or points in not compatible format.")
            return None
        except FileNotFoundError:
            extlog.error("File {0} not found.".format(os.path.abspath(__file__)))
            return None
        except ValueError:
            extlog.error("Input data is not valid")
            return None

    @pyqtSlot()
    def write_data_to_file(self):
        manual_points = self._dph.pop_manual_points()
        file = open(self._data_file, 'a')
        for x, y in manual_points.items():
            extlog.info("Writing ({0}, {1}) to '{2}'".format(x, y, self._data_file))
            file.write("\n{0} {1}".format(x, y))

    @pyqtSlot(QPointF)
    def add_manual_point(self, point):
        extlog.info("Adding point: [{0}, {1}]".format(point.x(), point.y()))
        self._dph.add_manual_point(point)

    @pyqtSlot(str)
    def process_changed_file(self, filename):
        extlog.info("File {0} changed".format(filename))
        self._dph.set_input_data(self.read_data_from_file(filename))

    @pyqtSlot(dict)
    def update_status(self, p_dict):
        extlog.info("Update point status:\n{0}".format(p_dict))
        self._ui.list_points.clear()
        for name, points in p_dict.items():
            if len(points) == 0:
                continue
            self._ui.list_points.addItem(name)
            for x, y in points.items():
                self._ui.list_points.addItem("[{0}, {1}]".format(x, y))

    def get_plot_item(self):
        return self._plot_widget.getPlotItem()

    def init_ui(self, ui_file):
        self._ui = uic.loadUi(uifile=ui_file)
        self._ui.plot_layout.addWidget(self._plot_widget)
        self._ui.show()

    @pyqtSlot(dict)
    def update_coefficients(self, params):
        spins_signal_blocker = SignalBlocker([
            self._ui.spin_a,
            self._ui.spin_b,
            self._ui.spin_c,
            self._ui.spin_d
        ])
        self._ui.spin_a.setValue(params['a'])
        self._ui.spin_b.setValue(params['b'])
        self._ui.spin_c.setValue(params['c'])
        self._ui.spin_d.setValue(params['d'])

        del spins_signal_blocker

        self.update_criterions(params)

    def update_criterions(self, params):
        self._ui.spin_mistake.setValue(params['mistake'])
        self._ui.spin_pirson.setValue(params['pirson'])
        self._ui.spin_crit_common.setValue(params['criterion'])

    balance_total = 100
    def balance_rate(self, mistake_rate, init_call=False):
        signal_blocker = SignalBlocker([
            self._ui.slider_mistake_rate,
            self._ui.spin_mistake_rate,
            self._ui.spin_pirson_rate
        ])
        self._ui.slider_mistake_rate.setValue(mistake_rate)
        self._ui.spin_mistake_rate.setValue(mistake_rate)
        self._ui.spin_pirson_rate.setValue(self.balance_total-mistake_rate)

        if not init_call:
            self._dph.set_mistake_rate(mistake_rate/float(self.balance_total))

        del signal_blocker

    @pyqtSlot(int)
    def set_slider_mistake_rate_value(self, val):
        self.balance_rate(val)

    @pyqtSlot(int)
    def set_mistake_rate_value(self, val):
        self.balance_rate(val)

    @pyqtSlot(int)
    def set_pirson_rate_value(self, val):
        self.balance_rate(self.balance_total - val)

    def init_signals_and_slots(self):
        self._watcher.fileChanged[str].connect(self.process_changed_file)
        self._view_box.clicked_here[QPointF].connect(self.add_manual_point)
        self._dph.points_updated[dict].connect(self.update_status)
        self._dph.coefficients_recalculated[dict].connect(self.update_coefficients)

        self._ui.spin_a.valueChanged[float].connect(self._dph.set_a)
        self._ui.spin_b.valueChanged[float].connect(self._dph.set_b)
        self._ui.spin_c.valueChanged[float].connect(self._dph.set_c)
        self._ui.spin_d.valueChanged[float].connect(self._dph.set_d)
        self._ui.button_calculate.clicked.connect(self._dph.recalc_abcd)
        self._ui.button_write_to_file.clicked.connect(self.write_data_to_file)

        self._ui.slider_mistake_rate.valueChanged[int].connect(self.set_slider_mistake_rate_value)
        self._ui.spin_mistake_rate.valueChanged[int].connect(self.set_mistake_rate_value)
        self._ui.spin_pirson_rate.valueChanged[int].connect(self.set_pirson_rate_value)


