from ext_logging import extlog
import sys

from pyqtgraph.Qt import QtGui
from MainWindow import MainWindow


if __name__ == '__main__':
    extlog.init_log()

    app = QtGui.QApplication(sys.argv)
    main_window = MainWindow("Forms/Plotter_main.ui")

    sys.exit(app.exec_())


