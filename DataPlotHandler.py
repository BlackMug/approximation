from PyQt5.QtCore import pyqtSignal, QObject, pyqtSlot
from PyQt5.QtGui import QColor

from ApproximationCurves import ApproxSin
from ext_logging import extlog
import pyqtgraph
import numpy as np


class ColorShuffler:

    class ColorListItem:
        color = None
        used = False

        def __init__(self, color):
            self.color = color

    colors = [
          ColorListItem(QColor(139, 139, 131))
        , ColorListItem(QColor(255, 0, 0))
        , ColorListItem(QColor(0, 191, 255))
        , ColorListItem(QColor(255, 255, 0))
        , ColorListItem(QColor(152, 251, 152))
        , ColorListItem(QColor(186, 85, 211))
        , ColorListItem(QColor(255, 165, 0))
        , ColorListItem(QColor(255, 215, 0))
        , ColorListItem(QColor(124, 252, 0))
        , ColorListItem(QColor(127, 255, 212))
        , ColorListItem(QColor(0, 0, 255))
        , ColorListItem(QColor(65, 105, 255))
        , ColorListItem(QColor(200, 200, 200))
        , ColorListItem(QColor(255, 255, 255))
    ]

    @staticmethod
    def get_unique_color():
        # shuffle(ColorShuffler.colors)
        for color_item in ColorShuffler.colors:
            if not color_item.used:
                color_item.used = True
                return color_item.color


class DataPlotHandler(QObject):
    points_updated = pyqtSignal(dict)
    coefficient_changed = pyqtSignal()
    coefficients_recalculated = pyqtSignal(dict)

    _plot_item = None

    _approx_sin = None

    _points_plot = None
    _approx_plot = None

    _file_points = {}
    _manual_points = {}

    def __init__(self, plot_item, parent=None):
        super().__init__(parent=parent)
        self._plot_item = plot_item

    def add_manual_point(self, point):
        try:
            self._manual_points[point.x()] = point.y()
            self.update_points()
        except FileExistsError:
            extlog.error("Input 'point' variable has no 'x' or 'y' field.")

    def set_input_data(self, input_data):
        if type(input_data) is not dict:
            extlog.error("'input_data' need to be 'dict' type, not {0}.".format(type(input_data)))
            return

        self._file_points = input_data
        self.update_points()

    def composite_data(self):
        ret_data = dict(self._file_points)
        ret_data.update(self._manual_points)
        return ret_data

    def request_coefficients_update(self):
        self.coefficients_recalculated.emit({
            'a': self._approx_sin.a,
            'b': self._approx_sin.b,
            'c': self._approx_sin.c,
            'd': self._approx_sin.d,
            'criterion': self._approx_sin.calc_criterion(self.composite_data()),
            'pirson': self._approx_sin.pirson,
            'mistake': self._approx_sin.mistake
        })

    @pyqtSlot()
    def recalc_abcd(self):
        data = self.composite_data()
        self._approx_sin.calc_abcd(data)
        self.request_coefficients_update()
        self.plot_approx(data)

    @pyqtSlot()
    def plot_approx(self, data=None):

        if not data:
            data = self.composite_data()
        x = list(data.keys())
        maxx = max(x)
        minx = min(x)

        if not self._approx_sin:
            self._approx_sin = ApproxSin(data)
            self.request_coefficients_update()
            self.coefficient_changed.connect(self.plot_approx)

        x = np.arange(minx, maxx, (maxx-minx)/1000)
        y = [self._approx_sin.f(xi) for xi in x]

        try:
            self._approx_plot.setData(x=x, y=y)
            self.request_coefficients_update()
        except AttributeError:
            self._approx_plot = self._plot_item.plot(
                x=x, y=y, pen=pyqtgraph.mkPen(ColorShuffler.get_unique_color(), width=1))

    def update_points(self):
        x = list(self._file_points.keys())
        y = list(self._file_points.values())
        x += list(self._manual_points.keys())
        y += list(self._manual_points.values())

        try:
            self._points_plot.setData(x=x, y=y)
        except AttributeError:
            self._points_plot = self._plot_item.scatterPlot(
                x=x, y=y, brush=pyqtgraph.mkBrush(ColorShuffler.get_unique_color(), width=1), size=5)
            self.plot_approx()

        self.points_updated.emit({
            'Points from file:': self._file_points,
            'Points on the fly:': self._manual_points
        })

    @pyqtSlot(str, float)
    def set_coefficient(self, coef_name, coef_val):
        set_funcs = {
            'a': lambda val: self._approx_sin.set_a(val),
            'b': lambda val: self._approx_sin.set_b(val),
            'c': lambda val: self._approx_sin.set_c(val),
            'd': lambda val: self._approx_sin.set_d(val)
        }
        try:
            set_funcs[coef_name](coef_val)
            self.coefficient_changed.emit()
        except ValueError:
            extlog.error("Something went wrong and you get undefined name for coefficient.")

    def set_a(self, val):
        self.set_coefficient('a', val)
    def set_b(self, val):
        self.set_coefficient('b', val)
    def set_c(self, val):
        self.set_coefficient('c', val)
    def set_d(self, val):
        self.set_coefficient('d', val)

    def pop_manual_points(self):
        manual_points = self._manual_points
        self._manual_points = {}
        return manual_points

    def get_mistake_rate(self):
        return self._approx_sin.mistake_rate_value

    def set_mistake_rate(self, mistake_rate):
        self._approx_sin.set_mistake_rate(mistake_rate)
